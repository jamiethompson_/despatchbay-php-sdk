<?php

namespace DespatchBay;

/**
 * Credentials
 * @package DespatchBay
 * @author Jamie Thompson
 *
 * Object representing a set of Despatch Bay API credentials
 * @see https://github.com/despatchbay/api.v14/wiki#authentication
 */
class Credentials
{
    /** @var string Despatch Bay API User token */
    public $apiUser;
    /** @var string Despatch Bay API Key */
    public $apiKey;

    public function __construct($apiUser, $apiKey)
    {
        $this->apiUser = $apiUser;
        $this->apiKey = $apiKey;
    }
}