<?php

namespace DespatchBay\Exception;

/**
 * PaymentException
 * @package DespatchBay\Exception
 * @author Jamie Thompson
 */
class PaymentException extends \Exception {}