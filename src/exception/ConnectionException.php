<?php

namespace DespatchBay\Exception;

/**
 * ConnectionException
 * @package DespatchBay\Exception
 * @author Jamie Thompson
 */
class ConnectionException extends \Exception {}