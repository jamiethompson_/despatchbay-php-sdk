<?php

namespace DespatchBay\Exception;

/**
 * AuthorizationException
 * @package DespatchBay\Exception
 * @author Jamie Thompson
 */
class AuthorizationException extends \Exception {}