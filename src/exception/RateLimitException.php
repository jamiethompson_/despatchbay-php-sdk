<?php

namespace DespatchBay\Exception;

/**
 * RateLimitException
 * @package DespatchBay\Exception
 * @author Jamie Thompson
 */
class RateLimitException extends \Exception {}