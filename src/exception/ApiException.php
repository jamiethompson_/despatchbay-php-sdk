<?php

namespace DespatchBay\Exception;

/**
 * ApiException
 * @package DespatchBay\Exception
 * @author Jamie Thompson
 */
class ApiException extends \Exception {}