<?php

namespace DespatchBay;
use DespatchBay\Entity\Pdf;
use DespatchBay\Library;

/**
 * Despatch Bay Client
 * @package DespatchBay
 * @author Jamie Thompson
 */
class Client extends Library\SoapClientWrapper
{
    /**
     * @api
     *
     * @return Entity\Account
     *
     * @throws Exception\AuthorizationException
     * @throws Exception\ConnectionException
     * @throws Exception\RateLimitException
     * @throws Exception\ApiException
     *
     * @see https://github.com/despatchbay/api.v14/wiki/Account-Service#getaccount
     */
    public function getAccount()
    {
        $object = $this->apiCall (
            self::SERVICE_ACCOUNT,
            __FUNCTION__,
            []
        );

        if ($object) {
            return new Entity\Account($object);
        }
    }

    /**
     * @api
     *
     * @return array of Entity\SenderAddress
     *
     * @throws Exception\AuthorizationException
     * @throws Exception\ConnectionException
     * @throws Exception\RateLimitException
     * @throws Exception\ApiException
     *
     * @see https://github.com/despatchbay/api.v14/wiki/Account-Service#getsenderaddresses
     */
    public function getSenderAddresses()
    {
        $array = $this->apiCall (
            self::SERVICE_ACCOUNT,
            __FUNCTION__,
            []
        );

        if (is_array($array)) {
            $entityArray = [];
            foreach ($array as $object) {
                $entityArray[] = new Entity\Sender($object);
            }
            return $entityArray;
        }
    }

    /**
     * @api
     *
     * @param string $postcode
     * @param string $property
     * @return Entity\Address
     *
     * @throws Exception\AuthorizationException
     * @throws Exception\ConnectionException
     * @throws Exception\RateLimitException
     * @throws Exception\ApiException
     *
     * @see https://github.com/despatchbay/api.v14/wiki/Addressing-Service#findaddress
     */
    public function findAddress($postcode, $property)
    {
        $response = $this->apiCall (
            self::SERVICE_ADDRESSING,
            __FUNCTION__,
            func_get_args()
        );
        return $response ? new Entity\Address($response) : null;
    }

    /**
     * @api
     *
     * @param $postcode
     * @return string[string] Array of unstructured address strings, indexed by address key
     *
     * @throws Exception\AuthorizationException
     * @throws Exception\ConnectionException
     * @throws Exception\RateLimitException
     * @throws Exception\ApiException
     *
     * @see https://github.com/despatchbay/api.v14/wiki/Addressing-Service#getaddresskeysbypostcode
     */
    public function getAddressKeysByPostcode($postcode)
    {
        $response = $this->apiCall (
            self::SERVICE_ADDRESSING,
            __FUNCTION__,
            func_get_args()
        );

        if ($response) {
            $array = [];
            foreach ($response as $object) {
                $array[$object->Key] = $object->Address;
            }
            return $array;
        }
    }

    /**
     * @api
     *
     * @param $addressKey
     * @return Entity\Address
     *
     * @throws Exception\AuthorizationException
     * @throws Exception\ConnectionException
     * @throws Exception\RateLimitException
     * @throws Exception\ApiException
     *
     * @see https://github.com/despatchbay/api.v14/wiki/Addressing-Service#getaddressbykey
     */
    public function getAddressByKey($addressKey)
    {
        $response = $this->apiCall (
            self::SERVICE_ADDRESSING,
            __FUNCTION__,
            func_get_args()
        );
        return $response ? new Entity\Address($response) : null;
    }

    /**
     * @api
     *
     * @param Entity\Sender $sender
     * @param Entity\Recipient $recipient
     * @param Entity\Parcel[] $parcels
     * @return Entity\Service[]|null
     *
     * @throws Exception\AuthorizationException
     * @throws Exception\ConnectionException
     * @throws Exception\RateLimitException
     * @throws Exception\ApiException
     *
     * @see https://github.com/despatchbay/api.v14/wiki/Shipping-Service#getavailableservices
     */
    public function getAvailableServices (
        Entity\Sender $sender,
        Entity\Recipient $recipient,
        $parcels
    ){
        $shipmentRequest = new Library\ShipmentRequestObject (
            $sender,
            $recipient,
            $parcels
        );

        $response = $this->apiCall (
            self::SERVICE_SHIPPING,
            __FUNCTION__,
            [$shipmentRequest]
        );

        $array = [];
        foreach ($response as $object) {
            $array[] = new Entity\Service($object);
        }
        return $array;
    }

    /**
     * @api
     *
     * Returns an array of DateTime objects (timezone Europe/London)
     *
     * @param Entity\Sender $sender
     * @param integer $courierId
     * @return \DateTime[]
     *
     * @throws Exception\AuthorizationException
     * @throws Exception\ConnectionException
     * @throws Exception\RateLimitException
     * @throws Exception\ApiException
     *
     * @see https://github.com/despatchbay/api.v14/wiki/Shipping-Service#getavailablecollectiondates
     */
    public function getAvailableCollectionDates (
        Entity\Sender $sender,
        $courierId
    ){
        $response = $this->apiCall (
            self::SERVICE_SHIPPING,
            __FUNCTION__,
            [
                $sender->toSoapObject(),
                $courierId
            ]
        );
        $tz = new \DateTimeZone(self::TIMEZONE);
        $dates = array();
        foreach ($response as $object) {
            array_push($dates, new \DateTime($object->CollectionDate, $tz));
        }
        return $dates;
    }

    /**
     * @api
     *
     * @param $shipmentId Shipment ID
     * @return Entity\Shipment
     *
     * @throws Exception\AuthorizationException
     * @throws Exception\ConnectionException
     * @throws Exception\RateLimitException
     * @throws Exception\ApiException
     *
     * @see https://github.com/despatchbay/api.v14/wiki/Shipping-Service#getshipment
     */
    public function getShipment($shipmentId)
    {
        $response = $this->apiCall (
            self::SERVICE_SHIPPING,
            __FUNCTION__,
            func_get_args()
        );
        return $response ? new Entity\Shipment($response) : null;
    }

    /**
     * @api
     *
     * @param Entity\Sender $sender
     * @param Entity\Recipient $recipient
     * @param Entity\Parcel[] $parcels
     * @param integer $serviceId
     * @param \DateTime $collectionDate
     * @param Entity\ShipmentOptions $options
     *
     * @return Entity\Shipment
     *
     * @throws Exception\AuthorizationException
     * @throws Exception\ConnectionException
     * @throws Exception\RateLimitException
     * @throws Exception\ApiException
     *
     * @see https://github.com/despatchbay/api.v14/wiki/Shipping-Service#addshipment
     */
    public function createShipment(
        Entity\Sender $sender,
        Entity\Recipient $recipient,
        $parcels,
        $serviceId,
        \DateTime $collectionDate,
        Entity\ShipmentOptions $options = null
    ){
        $requestObject = new Library\ShipmentRequestObject (
            $sender,
            $recipient,
            $parcels,
            $serviceId,
            $collectionDate,
            $options
        );

        $shipmentId = $this->apiCall (
            self::SERVICE_SHIPPING,
            'AddShipment',
            [$requestObject]
        );

        if ($shipmentId) {
            return $this->getShipment($shipmentId);
        }
    }

    /**
     * @api
     *
     * @param string $shipmentId Shipment ID
     * @return boolean
     *
     * @throws Exception\AuthorizationException
     * @throws Exception\ConnectionException
     * @throws Exception\RateLimitException
     * @throws Exception\ApiException
     *
     * @see https://github.com/despatchbay/api.v14/wiki/Shipping-Service#cancelshipment
     */
    public function cancelShipment($shipmentId)
    {
        $response = $this->apiCall (
            self::SERVICE_SHIPPING,
            'CancelShipment',
            [$shipmentId]
        );
        return $response ? true : false;
    }

    /**
     * @api
     *
     * @param string $shipmentId
     * @param string $format ('1A6'|'1A4'|'2A4')
     * @return Pdf
     *
     * @throws Exception\AuthorizationException
     * @throws Exception\ConnectionException
     * @throws Exception\RateLimitException
     * @throws Exception\ApiException
     */
    public function getLabels($shipmentId, $format = self::LABEL_FORMAT_1A6)
    {
        return $this->getPdfClient()->get($shipmentId, $format);
    }
}