<?php

namespace DespatchBay\Entity;
/**
 * SenderAddress
 * @package DespatchBay\Entity
 * @author Jamie Thompson
 * @see https://github.com/despatchbay/api.v14/wiki/Shipping-Service#senderaddresstype
 */
class Sender extends Contact
{
    protected $id;

    protected $soapMap = [
        'SenderName' => [
            'property' => 'name',
            'type' => 'string',
        ],
        'SenderTelephone' => [
            'property' => 'telephone',
            'type' => 'string',
        ],
        'SenderEmail' => [
            'property' => 'email',
            'type' => 'string',
        ],
        'SenderAddress' => [
            'property' => 'address',
            'type' => 'entity',
            'entityClass' => 'DespatchBay\Entity\Address',
        ],
        'SenderAddressID' => [
            'property' => 'id',
            'type' => 'integer',
        ],
    ];

    /**
     * Extend the default behaviour to deal with SenderAddressID overriding SenderAddress
     * @return \DespatchBay\Library\SoapObject
     */
    public function toSoapObject()
    {
        $object = parent::toSoapObject();
        if ($object->SenderAddressID) {
            $object->SenderAddress = null;
        }
        return $object;
    }
}