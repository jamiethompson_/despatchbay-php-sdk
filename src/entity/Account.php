<?php

namespace DespatchBay\Entity;
use DespatchBay\Library;
/**
 * Account
 * @package DespatchBay\Entity
 * @author Jamie Thompson
 * @see https://github.com/despatchbay/api.v14/wiki/Account-Service#accounttype
 *
 * @property integer $id Despatch Bay account ID
 * @property string $name Despatch Bay account name
 * @property AccountBalance $balance  Account balance
 */
class Account extends Library\Entity
{
    protected $id;
    protected $name;
    protected $balance;

    protected $soapMap = [
        'AccountID' => [
            'property' => 'id',
            'type' => 'integer'
        ],
        'AccountName' => [
            'property' => 'name',
            'type' => 'string'
        ],
        'AccountBalance' => [
            'property' => 'balance',
            'type' => 'entity',
            'entityClass' => 'DespatchBay\Entity\AccountBalance'
        ],
    ];
}