<?php

namespace DespatchBay\Entity;
use DespatchBay\Library;
/**
 * AddressKey
 * @package DespatchBay\Entity
 * @author Jamie Thompson
 * @see https://github.com/despatchbay/api.v14/wiki/Addressing-Service#addresskeytype
 *
 * @property string $key Unique key identifying the address
 * @property string $address Single-line unstructured string representation of the address
 */
class AddressKey extends Library\Entity
{
    protected $key;
    protected $address;

    protected $soapMap = [
        'Key' => [
            'property' => 'key',
            'type' => 'string',
        ],
        'Address' => [
            'property' => 'address',
            'type' => 'string',
        ],
    ];
}