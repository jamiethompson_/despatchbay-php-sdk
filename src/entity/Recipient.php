<?php

namespace DespatchBay\Entity;
/**
 * RecipientAddress
 * @package DespatchBay\Entity
 * @author Jamie Thompson
 * @see https://github.com/despatchbay/api.v14/wiki/Shipping-Service#recipientaddresstype
 */
class Recipient extends Contact
{
    protected $soapMap = [
        'RecipientName' => [
            'property' => 'name',
            'type' => 'string',
        ],
        'RecipientTelephone' => [
            'property' => 'telephone',
            'type' => 'string',
        ],
        'RecipientEmail' => [
            'property' => 'email',
            'type' => 'string',
        ],
        'RecipientAddress' => [
            'property' => 'address',
            'type' => 'entity',
            'entityClass' => 'DespatchBay\Entity\Address',
        ],
    ];
}