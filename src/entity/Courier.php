<?php

namespace DespatchBay\Entity;
use DespatchBay\Library;
/**
 * Courier
 * @package DespatchBay\Entity
 * @author Jamie Thompson
 * @see https://github.com/despatchbay/api.v14/wiki/Shipping-Service#couriertype
 *
 * @property integer $id Courier ID
 * @property string $name Courier name
 */
class Courier extends Library\Entity
{
    protected $id;
    protected $name;

    protected $soapMap = [
        'CourierID' => [
            'property' => 'id',
            'type' => 'integer'
        ],
        'CourierName' => [
            'property' => 'name',
            'type' => 'string'
        ]
    ];
}