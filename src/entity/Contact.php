<?php

namespace DespatchBay\Entity;
use DespatchBay\Library;
/**
 * ShippingAddress
 * @package DespatchBay\Entity
 * @author Jamie Thompson
 *
 * @property string $name Contact name
 * @property string $telephone Contact telephone number
 * @property string $email Contact email address
 * @property Address $address Address
 */
abstract class Contact extends Library\Entity
{
    protected $name;
    protected $telephone;
    protected $email;
    protected $address;
}