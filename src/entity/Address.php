<?php

namespace DespatchBay\Entity;
use DespatchBay\Library;
/**
 * Address
 * @package DespatchBay\Entity
 * @author Jamie Thompson
 * @see https://github.com/despatchbay/api.v14/wiki/Addressing-Service#addresstype
 *
 * @property string $companyName Company name
 * @property string $street Street
 * @property string $locality Locality
 * @property string $townCity Town or city
 * @property string $county County, State or Province
 * @property string $postalCode Postal code
 * @property string $countryCode ISO 3166-1 alpha-2 country code
 */
class Address extends Library\Entity
{
    protected $companyName;
    protected $street;
    protected $locality;
    protected $townCity;
    protected $county;
    protected $postalCode;
    protected $countryCode;

    protected $soapMap = [
        'CompanyName' => [
            'property' => 'companyName',
            'type' => 'string',
        ],
        'Street' => [
            'property' => 'street',
            'type' => 'string',
        ],
        'Locality' => [
            'property' => 'locality',
            'type' => 'string',
        ],
        'TownCity' => [
            'property' => 'townCity',
            'type' => 'string',
        ],
        'County' => [
            'property' => 'county',
            'type' => 'string',
        ],
        'PostalCode' => [
            'property' => 'postalCode',
            'type' => 'string',
        ],
        'CountryCode' => [
            'property' => 'countryCode',
            'type' => 'string',
        ],
    ];
}