<?php

namespace DespatchBay\Entity;
use DespatchBay\Library;
/**
 * AccountBalance
 * @package DespatchBay\Entity
 * @author Jamie Thompson
 * @see https://github.com/despatchbay/api.v14/wiki/Account-Service#accountbalancetype
 *
 * @property float $balance Account balance
 * @property float $available Available balance
 */
class AccountBalance extends Library\Entity
{
    protected $balance;
    protected $available;

    protected $soapMap = [
        'Balance' => [
            'property' => 'balance',
            'type' => 'float'
        ],
        'AvailableBalance' => [
            'property' => 'available',
            'type' => 'float'
        ],
    ];
}