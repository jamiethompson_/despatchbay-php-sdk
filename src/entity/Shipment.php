<?php

namespace DespatchBay\Entity;
use DespatchBay\Client;
use DespatchBay\Library;
/**
 * Shipment
 * @package DespatchBay\Entity
 * @author Jamie Thompson
 * @see https://github.com/despatchbay/api.v14/wiki/Shipping-Service#shipmentreturntype
 *
 * @property string $id Shipment ID
 * @property integer $serviceId Service ID
 * @property Parcel[] $parcels Array of parcels
 * @property string $reference Customer reference
 * @property Address $recipientAddress The recipient address
 * @property boolean $isFollowed Followed flag
 * @property boolean $isPrinted Printed flag
 * @property boolean $isDespatched Despatched flag
 * @property boolean $isDelivered Delivered flag
 * @property boolean $isCancelled Cancelled flag
 */
class Shipment extends Library\Entity
{
    protected $id;
    protected $serviceId;
    protected $parcels;
    protected $reference;
    protected $recipientAddress;
    protected $isFollowed;
    protected $isPrinted;
    protected $isDespatched;
    protected $isDelivered;
    protected $isCancelled;

    protected $soapMap = [
        'ShipmentID' => [
            'property' => 'id',
            'type' => 'string',
        ],
        'ServiceID' => [
            'property' => 'serviceId',
            'type' => 'string',
        ],
        'Parcels' => [
            'property' => 'parcels',
            'type' => 'entityArray',
            'entityClass' => 'DespatchBay\Entity\Parcel',
        ],
        'ClientReference' => [
            'property' => 'reference',
            'type' => 'string',
        ],
        'RecipientAddress' => [
            'property' => 'recipientAddress',
            'type' => 'entity',
            'entityClass' => 'DespatchBay\Entity\Recipient',
        ],
        'IsFollowed' => [
            'property' => 'isFollowed',
            'type' => 'boolean',
        ],
        'IsPrinted' => [
            'property' => 'isPrinted',
            'type' => 'boolean',
        ],
        'IsDespatched' => [
            'property' => 'isDespatched',
            'type' => 'boolean',
        ],
        'IsDelivered' => [
            'property' => 'isDelivered',
            'type' => 'boolean',
        ],
        'IsCancelled' => [
            'property' => 'isCancelled',
            'type' => 'boolean',
        ],
    ];

    public function cancel()
    {
        return \DespatchBay\Client::getInstance()->cancelShipment($this->id);
    }

    /**
     * @param string $format
     * @return Pdf PDF document
     */
    public function getLabels($format = Client::LABEL_FORMAT_1A6)
    {
        return \DespatchBay\Client::getInstance()->getLabels($this->id, $format);
    }
}