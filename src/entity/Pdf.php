<?php

namespace DespatchBay\Entity;
use DespatchBay\Exception;
/**
 * Pdf
 * @package DespatchBay\Entity
 * @author Jamie Thompson
 */
class Pdf
{
    private $data;

    public function __construct($data)
    {
        if (!$this->isPdf($data)) {
            throw new \InvalidArgumentException('PDF data is invalid');
        } else {
            $this->data = $data;
        }
    }

    /**
     * @internal
     *
     * Provides a very crude check that string data _appears_ to be a valid PDF
     * This is more of a shotgun sanity check than a belt-and-braces validation
     *
     * @param string $data
     * @return bool
     */
    private function isPdf($data)
    {
        return substr($data,0,4) == '%PDF';
    }

    /**
     * @internal
     * Set up standard headers required for successful PDF transfer
     * @return void
     */
    private function sendHeaders()
    {
        header('content-type: application/pdf');
        header('Content-Transfer-Encoding: binary');
        header('Cache-Control: private, max-age=0, must-revalidate');
        header('Pragma: public');

        /* zlib output compression can break PDF downloads in Internet Explorer */
        if (ini_get('zlib.output_compression')) {
            ini_set('zlib.output_compression', 0);
        }
    }

    /**
     * @api
     * Returns the raw PDF file data as a string.
     * @return string
     */
    public function getRaw()
    {
        return $this->data;
    }

    /**
     * @api
     * Returns a base64 encoded representation of the PDF file data
     * @return string
     */
    public function getBase64()
    {
        return base64_encode($this->getRaw());
    }

    /**
     * @api
     * Send the file inline to the browser. The browser will invoke a PDF viewer if available.
     * Note: This method will cause a "headers already sent" error if preceding code has sent some headers
     * @return void
     */
    public function output()
    {
        $this->sendHeaders();
        header('Content-Length: ' . strlen($this->data));
        echo $this->data;
    }

    /**
     * @api
     *
     * Forces a file download with the filename specified.
     * A random filename will be automatically generated if this is omitted
     *
     * @param string|null $filename
     * @return void
     */
    public function download($filename = null)
    {
        $this->sendHeaders();
        $filename = $filename ?: sprintf('%s.pdf', uniqid('pdf'));
        header(sprintf('Content-Disposition: attachment; filename="%s.pdf"', $filename));
        echo $this->data;
    }
}