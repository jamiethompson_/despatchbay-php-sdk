<?php

namespace DespatchBay\Entity;
use DespatchBay\Library;
/**
 * ShipmentOptions
 * @package DespatchBay\Entity
 * @author Jamie Thompson
 *
 * @property boolean $follow isFollowed flag
 * @property string $reference Client reference
 */
class ShipmentOptions extends Library\Entity
{
    protected $follow;
    protected $reference;

    /**
     * @internal
     * ShipmentOptions does not represent an actual SoapObject
     * @throws \LogicException
     */
    public function toSoapObject()
    {
        throw new \LogicException('toSoapObject cannot be called on the entity ' . __CLASS__);
    }
}