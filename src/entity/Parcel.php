<?php

namespace DespatchBay\Entity;
use DespatchBay\Library;
/**
 * Parcel
 * @package DespatchBay\Entity
 * @author Jamie Thompson
 * @see https://github.com/despatchbay/api.v14/wiki/Shipping-Service#parceltype
 *
 * @property float $weight The weight of the parcel in kg
 * @property float $length The length of the parcel in cm (longest dimension)
 * @property float $width The width of the parcel in cm (second longest dimension)
 * @property float $height The height of the parcel in cm (shortest dimension)
 * @property string $contents Description of the contents of the parcel
 * @property float $value Monetary value of the contents of the parcel (in GBP)
 * @property string $trackingNumber The parcel tracking number
 */
class Parcel extends Library\Entity
{
    protected $weight;
    protected $length;
    protected $width;
    protected $height;
    protected $contents;
    protected $value;
    protected $trackingNumber;

    protected $soapMap = [
        'Weight' => [
            'property' => 'weight',
            'type' => 'float'
        ],
        'Length' => [
            'property' => 'length',
            'type' => 'float'
        ],
        'Width' => [
            'property' => 'width',
            'type' => 'float'
        ],
        'Height' => [
            'property' => 'height',
            'type' => 'float'
        ],
        'Contents' => [
            'property' => 'contents',
            'type' => 'string'
        ],
        'Value' => [
            'property' => 'value',
            'type' => 'float'
        ],
        'TrackingNumber' => [
            'property' => 'trackingNumber',
            'type' => 'string'
        ],
    ];
}