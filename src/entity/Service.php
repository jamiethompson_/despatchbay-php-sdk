<?php

namespace DespatchBay\Entity;
use DespatchBay\Library;
/**
 * Service
 * @package DespatchBay\Entity
 * @author Jamie Thompson
 * @see https://github.com/despatchbay/api.v14/wiki/Shipping-Service#servicetype
 *
 * @property integer $id Shipping Service ID
 * @property string $name Shipping Service name
 * @property float $cost Cost of Shipping Service
 * @property Courier $courier The Courier which provides the Shipping Service
 */
class Service extends Library\Entity
{
    protected $id;
    protected $name;
    protected $cost;
    protected $courier;

    protected $soapMap = [
        'ServiceID' => [
            'property' => 'id',
            'type' => 'integer'
        ],
        'Name' => [
            'property' => 'name',
            'type' => 'string'
        ],
        'Cost' => [
            'property' => 'cost',
            'type' => 'currency',
        ],
        'Courier' => [
            'property' => 'courier',
            'type' => 'entity',
            'entityClass' => 'DespatchBay\Entity\Courier'
        ],
    ];
}