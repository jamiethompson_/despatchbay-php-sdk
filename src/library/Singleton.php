<?php

namespace DespatchBay\Library;

/**
 * Singleton
 * @package DespatchBay\Library
 * @author Jamie Thompson
 */
class Singleton {

    protected static $instance;
    private function __clone() {}
    private function __construct() {}
    private function __wakeup() {}

    /**
     * @return static
     */
    final public static function getInstance() {
        if (!isset( static::$instance)) {
            static::$instance = new static();
        }
        return static::$instance;
    }
}