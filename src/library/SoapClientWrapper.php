<?php

namespace DespatchBay\Library;
use DespatchBay\Credentials;
use DespatchBay\Exception;
use GuzzleHttp;
/**
 * Despatch Bay Api Client
 * @package DespatchBay\Library
 * @author Jamie Thompson
 */
class SoapClientWrapper extends Singleton
{
    const CLIENT_VERSION     = '0.0.0';
    const API_VERSION_SOAP   = 14;
    const TIMEZONE           = 'Europe/London';
    const WSDL_TEMPLATE      = 'http://api.despatchbay.com/soap/v%d/%s?wsdl';

    const SERVICE_ACCOUNT    = 'account';
    const SERVICE_ADDRESSING = 'addressing';
    const SERVICE_SHIPPING   = 'shipping';
    const SERVICE_TRACKING   = 'tracking';

    const LABEL_FORMAT_1A6   = '1A6';
    const LABEL_FORMAT_1A4   = '1A4';
    const LABEL_FORMAT_2A4   = '2A4';

    /** @var Credentials Despatch Bay API credentials */
    private $credentials;
    /** @var string SOAP Client class name   */
    private $soapClientClass = \SoapClient::class;
    /** @var string Custom user agent string */
    private $applicationIdentifier;
    /** @var \SoapClient[] Holds a single SOAP client instance for each service */
    private $soapClients = ['account','addressing' ,'shipping','tracking'];

    /**
     * @api
     * Sets the client credentials. This method must be called prior to using the client methods.
     * Credentials take effect for all following API requests
     * @see https://github.com/despatchbay/api.v14/wiki#authentication
     *
     * @param Credentials $credentials
     */
    public function setCredentials(Credentials $credentials)
    {
        if ($credentials instanceof Credentials) {
            $this->destroySoapClients();
        }
        $this->credentials = $credentials;
    }

    /**
     * @return Credentials
     */
    public function getCredentials()
    {
        if ($this->credentials instanceof Credentials) {
            return $this->credentials;
        }
    }

    /**
     * @api
     * Sets a custom user-agent string which the client will use to communicate with the Despatch Bay API
     * By default the client sends a UA string in the format `PHP-SDK/n.n.n (PHP n.n.n; OS n.n.n)`
     * Setting a custom user agent will result in a UA string in the following format:
     * `YourCustomString (PHP-SDK/n.n.n; PHP n.n.n; OS n.n.n)`
     *
     * @param $string string custom user-agent string
     */
    public function setApplicationIdentifier($string)
    {
        $this->applicationIdentifier = $string;
    }

    /**
     * @internal
     *
     * Allows for overriding the default SoapClient class for testing purposes.
     * _Incorrect usage of this function will lead to failure_
     *
     * @param string $class class name of desired SoapClient class
     */
    public function setSoapClientClass($class)
    {
        $this->soapClientClass = $class;
    }

    /**
     * @internal
     *
     * API call wrapper method
     *
     * @param $service
     * @param $method
     * @param $args
     * @return mixed
     * @throws Exception\AuthorizationException
     * @throws Exception\ConnectionException
     * @throws Exception\RateLimitException
     * @throws Exception\ApiException
     */
    protected function apiCall($service, $method, $args)
    {
        $client = $this->getSoapClient($service);
        try {
            $result = $client->__soapCall($method, $args);
            return $result;
        } catch (\SoapFault $e) {
            $this->handleSoapFault($e);
        }
    }

    /**
     * @internal
     * Destroys a SoapClient instance
     * @param $service
     */
    private function destroySoapClient($service)
    {
        $this->soapClients[$service] = null;
    }

    /**
     * @internal
     * Destroys all SoapClient instances
     */
    private function destroySoapClients()
    {
        foreach (array_keys($this->soapClients) as $service) {
            $this->destroySoapClient($service);
        }
    }

    /**
     * @internal
     * Returns a User-Agent string
     * @return string User-Agent string
     */
    protected function getUserAgent()
    {
        $template = $this->applicationIdentifier ? '%s (PHP-SDK/%s; PHP %s; %s %s)' : '%sPHP-SDK/%s (PHP %s; %s %s)';

        return sprintf(
            $template,
            $this->applicationIdentifier,
            self::CLIENT_VERSION,
            PHP_VERSION,
            php_uname('s'),
            php_uname('r')
        );
    }

    /**
     * @internal
     * @return array of SoapClient options
     */
    private function getOptions()
    {
        return [
            'cache_wsdl' => true,
            'login' => $this->credentials->apiUser,
            'password' => $this->credentials->apiKey,
            'user_agent' => $this->getUserAgent(),
            'exceptions' => true
        ];
    }

    /**
     * @internal
     * @return bool indicating whether credentials have been set
     */
    private function hasCredentials()
    {
        return $this->credentials instanceof Credentials;
    }

    /**
     * @internal
     * Factory-style method for returning singleton-esque SoapClient instances
     * @param $serviceName
     * @return \SoapClient
     * @throws Exception\AuthorizationException
     * @throws Exception\ConnectionException
     */
    private function getSoapClient($serviceName)
    {
        if (!$this->hasCredentials()) {
            throw new Exception\AuthorizationException('Missing API credentials');
        }

        if (!isset($this->soapClients[$serviceName]) ||
            !($this->soapClients[$serviceName] instanceof $this->soapClientClass)) {

            $url = $this->getServiceUrl($serviceName);

            if (!$this->testConnection($url)) {
                throw new Exception\ConnectionException (
                    sprintf("Failed to connect to the Despatch Bay %s API", ucfirst($serviceName))
                );
            }

            try {
                $this->soapClients[$serviceName] = new $this->soapClientClass($url, $this->getOptions());
            } catch (\SoapFault $exception) {
                throw new Exception\ConnectionException (
                    sprintf("Failed to connect to the Despatch Bay %s API", ucfirst($serviceName)),
                    null,
                    $exception
                );
            }
        }
        return $this->soapClients[$serviceName];
    }

    private function testConnection($url)
    {
        $h = curl_init($url);
        curl_setopt($h, CURLOPT_RETURNTRANSFER, TRUE);
        $response = curl_exec($h);
        $success = curl_getinfo($h, CURLINFO_HTTP_CODE) == 200;
        curl_close($h);
        return $success;
    }

    /**
     * @return PdfClient
     */
    protected function getPdfClient()
    {
        return new PdfClient($this->getCredentials(), $this->getUserAgent());
    }

    /**
     * @internal
     * @param string $serviceName
     * @return string WSDL URI using the template, API version number and a service name
     */
    private function getServiceUrl($serviceName)
    {
        return sprintf(
            self::WSDL_TEMPLATE,
            self::API_VERSION_SOAP,
            $serviceName
        );
    }

    /**
     * @internal
     * Handles SoapFaults returned by the API
     * @param \SoapFault $exception
     * @throws Exception\AuthorizationException
     * @throws Exception\ConnectionException
     * @throws Exception\RateLimitException
     * @throws Exception\ApiException
     */
    private function handleSoapFault(\SoapFault $exception)
    {
        switch ($exception->getMessage()) {
            case "Unauthorized":
                throw new Exception\AuthorizationException (
                    'Invalid API credentials',
                    null,
                    $exception
                );
                break;
            case "Could not connect to host":
                throw new Exception\ConnectionException (
                    'Failed to connect to the Despatch Bay API',
                    null,
                    $exception
                );
                break;
            case "Your access rate limit for this service has been exceeded":
                throw new Exception\RateLimitException (
                    $exception->getMessage(),
                    null,
                    $exception
                );
            default:
                // Re-throw any general SoapFault Exceptions as Exception\ApiException
                throw new Exception\ApiException (
                    $exception->getMessage(),
                    $exception->getCode(),
                    $exception
                );
        }
    }
}