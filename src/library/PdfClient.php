<?php

namespace DespatchBay\Library;
use DespatchBay\Entity\Pdf;
use DespatchBay\Exception;

/**
 * PdfClient
 * @package DespatchBay\Library
 * @author Jamie Thompson
 */
class PdfClient
{
    const API_URI   = 'http://api.despatchbay.st/pdf/1.0.1/labels?%s';

    private $curlHandle;
    private $auth;
    private $userAgent;

    /**
     * PdfClient constructor.
     * @param \DespatchBay\Credentials $credentials
     * @param string $userAgent
     */
    public function __construct(\DespatchBay\Credentials $credentials, $userAgent)
    {
        $this->auth = [
            'apiuser' => $credentials->apiUser,
            'apikey'  => $credentials->apiKey
        ];
        $this->userAgent = $userAgent;
    }

    /**
     * @return bool
     */
    private function curlCheck()
    {
        return function_exists('curl_init');
    }

    /**
     * @param string $shipmentId
     * @param string $format ('1A6'|'1A4'|'2A4')
     * @return Pdf PDF document
     *
     * @throws Exception\ApiException
     */
    private function fetchPdf($shipmentId, $format)
    {
        $this->curlInit();
        $params = [
            'sid' => $shipmentId,
            'format' => $format
        ];
        $params = http_build_query(array_merge($params, $this->auth));
        curl_setopt($this->curlHandle, CURLOPT_URL, sprintf(self::API_URI, $params));

        $data = curl_exec($this->curlHandle);
        $responseCode = curl_getinfo($this->curlHandle, CURLINFO_HTTP_CODE);
        curl_close($this->curlHandle);
        $this->curlHandle = null;

        $this->handleResponseCode($responseCode);

        try {
            $pdf = new Pdf($data);
        } catch (\Exception $exception) {
            throw new Exception\ApiException('Invalid PDF data received from API', '', $exception);
        }
        return $pdf;
    }

    /**
     * @throws \Exception
     */
    private function curlInit()
    {
        if (!$this->curlCheck()) {
            throw new \Exception('cURL is not installed');
        }
        $this->curlHandle = curl_init();
        curl_setopt($this->curlHandle, CURLOPT_USERAGENT, $this->userAgent);
        curl_setopt($this->curlHandle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->curlHandle, CURLOPT_TIMEOUT, 30);
    }

    private function handleResponseCode($code)
    {
        switch ($code) {
            case 200:
                return true;
            case 400:
                throw new \InvalidArgumentException('The PDF Labels API was unable to process the request');
                break;
            case 401:
                throw new Exception\AuthorizationException('Unauthorized');
                break;
            case 402:
                throw new Exception\PaymentException('Insufficient Despatch Bay account balance');
                break;
            case 404:
                throw new Exception\ApiException('Unknown shipment ID');
                break;
            default:
                throw new Exception\ApiException(sprintf('An unexpected error occurred (HTTP %d)', $code));
                break;
        }
    }

    /**
     * @param string $shipmentId
     * @param string $format ('1A6'|'1A4'|'2A4')
     * @return Pdf
     *
     * @throws Exception\ApiException
     * @throws Exception\AuthorizationException
     * @throws Exception\PaymentException
     * @throws \InvalidArgumentException
     * @throws \Exception
     */
    public function get($shipmentId, $format)
    {
        if (!in_array($format, [
            SoapClientWrapper::LABEL_FORMAT_1A6,
            SoapClientWrapper::LABEL_FORMAT_1A4,
            SoapClientWrapper::LABEL_FORMAT_2A4
        ])) {
            throw new \InvalidArgumentException('An invalid label format was passed');
        }

        $result = $this->fetchPdf($shipmentId, $format);
        return $result;
    }
}