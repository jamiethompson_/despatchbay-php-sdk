<?php

namespace DespatchBay\Library;

/**
 * Entity
 * @package DespatchBay\Library
 * @author Jamie Thompson
 */
abstract class Entity
{
    /**
     * Entity constructor
     * @param \stdClass|null $soapObject
     */
    public function __construct(\stdClass $soapObject = null)
    {
        if (!is_null($soapObject)) { $this->populate($soapObject); };
    }

    /**
     * @var array Maps SOAP elements to entity properties
     */
    protected $soapMap;

    /**
     * Populates an entity from a SoapObject
     * @param \stdClass $soapObject
     * @return mixed
     */
    public function populate(\stdClass $soapObject)
    {
        foreach ($this->soapMap as $element => $mapping) {
            if (!isset($soapObject->$element)) { continue; }
            $property = $mapping['property'];
            switch ($mapping['type'])
            {
                case 'entity':
                    $entityClass = $mapping['entityClass'];
                    $this->$property = new $entityClass($soapObject->$element);
                    break;
                case 'entityArray':
                    $array = [];
                    $entityClass = $mapping['entityClass'];
                    foreach ($soapObject->$element as $object) {
                        $array[] = new $entityClass($object);
                    }
                    $this->$property = $array;
                    break;
                default:
                    $this->$property = $soapObject->$element;
            }
        }
    }

    /**
     * Returns a SoapObject representation of an entity
     * @return SoapObject
     */
    public function toSoapObject()
    {
        $object = new SoapObject();
        foreach ($this->soapMap as $element => $mapping) {
            $property = $mapping['property'];
            switch ($mapping['type'])
            {
                case 'entity':
                    $object->$element = $this->$property->toSoapObject();
                    break;
                case 'entityArray':
                    $array = [];
                    foreach ($this->$property as $entity) {
                        $array[] = $entity->toSoapObject();
                    }
                    $object->$element = $array;
                    break;
                default:
                    $object->$element = $this->$property;
            }
        }
        return $object;
    }

    /**
     * Sets a property of an entity
     * @param mixed $property
     * @param mixed $value
     * @return void
     */
    public function __set($property, $value)
    {
        if (!property_exists($this, $property)) {
            throw new \InvalidArgumentException(
                sprintf(
                    '%s does not have a property named %s',
                    get_class($this),
                    $property
                )
            );
        }
        //echo $property;
        $this->$property = $value;
    }

    /**
     * Gets a property of an entity
     * @param mixed $property
     * @return mixed
     */
    public function __get($property)
    {
        if (!property_exists($this, $property)) {
            throw new \InvalidArgumentException(
                sprintf(
                    '%s does not have a property named %s',
                    get_class($this),
                    $property
                )
            );
        }
        return $this->$property;
    }
}
