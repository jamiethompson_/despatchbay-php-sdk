<?php

namespace DespatchBay\Library;
use DespatchBay\Entity as Entities;
/**
 * ShipmentRequestObject
 * @package DespatchBay\Library
 * @author Jamie Thompson
 */
class ShipmentRequestObject extends SoapObject
{
    public $ServiceID;
    public $SenderAddress;
    public $RecipientAddress;
    public $Parcels;
    public $CollectionDate;
    public $FollowShipment;
    public $ClientReference;

    /**
     * ShipmentRequestObject constructor.
     * @param \DespatchBay\Entity\Sender $sender
     * @param \DespatchBay\Entity\Recipient $recipient
     * @param \DespatchBay\Entity\Parcel[] $parcels
     * @param integer|null $serviceId
     * @param \DateTime|null $collectionDate
     * @param \DespatchBay\Entity\ShipmentOptions|null $options
     */
    public function __construct (
        Entities\Sender $sender,
        Entities\Recipient $recipient,
        $parcels,
        $serviceId = null,
        \DateTime $collectionDate = null,
        Entities\ShipmentOptions $options = null
    )
    {
        $this->SenderAddress = $sender->toSoapObject();
        $this->RecipientAddress = $recipient->toSoapObject();

        $this->Parcels = [];
        foreach ($parcels as $parcel) { $this->Parcels[] = $parcel->toSoapObject(); }

        if (!is_null($serviceId)) {
            $this->ServiceID = $serviceId;
        }
        if (!is_null($collectionDate) && $collectionDate instanceof \DateTime) {
            $this->CollectionDate = new SoapObject;
            $this->CollectionDate->CollectionDate = $collectionDate->format('Y-m-d');
        }

        if (!is_null($options) && $options instanceof Entities\ShipmentOptions) {
            $this->ClientReference = $options->reference;
            $this->FollowShipment = $options->follow;
        }
    }
}